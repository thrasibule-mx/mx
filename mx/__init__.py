# mx/__init__.py
# ==============
#
# Copying
# -------
#
# Copyright (c) 2021 mx authors and contributors.
#
# This file is part of the *mx* project.
#
# mx is a free software project. You can redistribute it and/or modify it
# following the terms of the MIT License.
#
# This software project is distributed *as is*, WITHOUT WARRANTY OF ANY KIND;
# including but not limited to the WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE and NONINFRINGEMENT.
#
# You should have received a copy of the MIT License along with *mx*. If not,
# see <http://opensource.org/licenses/MIT>.
#
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
